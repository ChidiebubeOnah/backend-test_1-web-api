﻿using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using Backend_task_one__online_store_api_.Interfaces;
using Backend_task_one__online_store_api_.Persistence;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Backend_task_one__online_store_api_.Repositories
{
    public class VendorRepository: IVendorRepository
    {
        private readonly AppDbContext _context;
 
        public VendorRepository(AppDbContext context)
        {
            _context = context;
        }
        
        public IEnumerable<Vendor> GetVendors()
        {

            return _context.Vendors.Find(new BsonDocument()).ToList();
        }

      
        public IEnumerable<Vendor> GetVendors(string search)
        {
            return _context.Vendors.Find(v => v.Name.ToLower().Contains(search.ToLower())).ToList();
        }

        public Vendor GetVendorById(string id)
        {
            return _context.Vendors.Find(v => v.Id == id).SingleOrDefault();
        }

        public void CreateVendor(Vendor vendor)
        {
            _context.Vendors.InsertOne(vendor);
        }

        public void UpdateVendor(string id, Vendor vendor)
        {
            _context.Vendors.ReplaceOne(v => v.Id == id, vendor);
        }

        public void RemoveVendor(string id)
        {
            _context.Vendors.DeleteOne(v => v.Id == id);
        }
    }
}
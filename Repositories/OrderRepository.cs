﻿using System;
using System.Collections.Generic;
using Backend_task_one__online_store_api_.Interfaces;
using Backend_task_one__online_store_api_.Models;
using Backend_task_one__online_store_api_.Persistence;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Backend_task_one__online_store_api_.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IMongoClient _client;
        private readonly AppDbContext _context;
        private readonly IProductRepository _productRepo;
        private readonly ICustomerRepository _customerRepo;

        public OrderRepository(IMongoClient client, AppDbContext context, IProductRepository productRepo, ICustomerRepository customerRepo)
        {
            _client = client;
            _context = context;
            _productRepo = productRepo;
            _customerRepo = customerRepo;
        }
        
        public IEnumerable<Order> GetOrders()
        {
            return _context.Orders.Find(new BsonDocument()).ToList();
        }

       

        public Order GetOrder(string orderId)
        {
            return _context.Orders.Find(o => o.Id == orderId).SingleOrDefault();
        }

        public (bool success, string msg )MakeOrder(Order order)
        {

            //Initiate A session
            using (var session = _client.StartSession())
            {
                
                //Begin Transaction
                session.StartTransaction();

                var customer = _context.Customers.Find(c => c.Id == order.CustomerId).SingleOrDefault();
                var product = _context.Products.Find(p=> p.Id == order.ProductId).SingleOrDefault();

                if (customer == null && product == null)
                {
                    session.AbortTransaction();
                    return (false, "Either productId or customerId doesn't exists!!!");
                }

                if (product.Quantity <= 0)
                {
                    session.AbortTransaction();
                    return (false, "The Product is Out of Stock!!!");
                }
            

                var productPrice = product.Price;
                var orderQuantity = order.Quantity;

                //Update Total field of the Orders Table
                order.Total = productPrice * orderQuantity;

                //reduce the quantity of the product in stock
                product.Quantity -= orderQuantity;
                product.TotalOrder += orderQuantity;

                //Checks for any Db errors
                try
                {

                    // Run the Db Query
                    _productRepo.UpdateProduct(product.Id, product);

                    _context.Orders.InsertOne(order);

                    customer?.OrderIds.Add(order.Id);
                    _customerRepo.UpdateCustomer(customer?.Id, customer);


                    session.CommitTransaction();

                    return (true, "Done");

                }
                catch (Exception e)
                {
                    //Cancel Operation
                    session.AbortTransaction();
                   return (false, e.Message);
                }


            }

        }

        public void UpdateOrder(string id, Order order)
        {

            _context.Orders.ReplaceOne(o => o.Id == id, order);

            var product = _context.Products.Find(p => p.Id == order.ProductId).SingleOrDefault();

            //reduce the quantity of the product in stock
            product.Quantity -= order.Quantity;

            _productRepo.UpdateProduct(order.ProductId, product);

        }
        public void RemoveOrder(string id)
        {
            _context.Orders.DeleteOne(o => o.Id == id);
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Backend_task_one__online_store_api_.Interfaces;
using Backend_task_one__online_store_api_.Models;
using Backend_task_one__online_store_api_.Persistence;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Backend_task_one__online_store_api_.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly AppDbContext _context;
       

        public ProductRepository(AppDbContext context)
        {
            _context = context;
        }


        public IEnumerable<ProductViewData> GetProductsViewData()
        {
            var vendors = _context.Vendors.AsQueryable();
            var products = _context.Products.AsQueryable();

            // Products Table Joined With Vendors
            var joinedTable = products.Join(
                vendors,
                product => product.VendorId,
                vendor => vendor.Id,
                (product, vendor) => new {product, vendor})
                .ToList()
                .Select(x => new ProductViewData(x.product, x.vendor));

            return joinedTable;

        }

        public IEnumerable<ProductViewData> GetProductsViewData(string search)
        {
            return GetProductsViewData()
                .Where(p => p.Name.ToLower()
                .Contains(search.ToLower()) || p.Category.Name.ToLower().Contains(search.ToLower()))
                .ToList();

        }

        public Product GetProductById(string id)
        {
            return _context.Products.Find(p => p.Id == id).SingleOrDefault();
        }
        public ProductViewData GetProductViwDataById(string id)
        {
           return GetProductsViewData().SingleOrDefault(p => p.Id == id);
        }
       

        public void AddProduct(Product product)
        {
            _context.Products.InsertOne(product);
        }

        public void UpdateProduct(string id, Product product)
        {
            _context.Products.ReplaceOne(p => p.Id == id, product);
        }

        public void RemoveProduct(string id)
        {
            _context.Products.DeleteOne(p => p.Id == id);
        }

        public IEnumerable GetProductCategorySales()
        {
            /*
             * Displays Expected Total Cost
             * of All Products in each Category
             * in Descending Order
             */

            return _context.Products.AsQueryable().GroupBy(p => p.Category.Name, (cat, product) => new
                {
                    category = cat,
                    salesExpected = product.Select(x => x.Price * x.Quantity).Sum()
                }
            ).OrderByDescending(x => x.salesExpected).ToList();


        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend_task_one__online_store_api_.Interfaces;
using Backend_task_one__online_store_api_.Models;
using Backend_task_one__online_store_api_.Persistence;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Backend_task_one__online_store_api_.Repositories
{
    public class CustomerRepository: ICustomerRepository
    {
        private readonly AppDbContext _context;


        public CustomerRepository(AppDbContext context)
        {
            _context = context;
           
        }
        public IEnumerable<Customer> GetCustomers()
        {
             return _context.Customers.Find(new BsonDocument()).ToList();
        }

        public IEnumerable<Customer> GetCustomers(string search)
        {
            return _context.Customers.Find(c => c.FullName.ToLower().Contains(search.ToLower())).ToList(); 
        }

        public Customer GetCustomerById(string id)
        {
            return _context.Customers.Find(c => c.Id == id).SingleOrDefault();
        }

        public void CreateCustomer(Customer customer)
        {
            _context.Customers.InsertOne(customer);
        }

        public void UpdateCustomer(string id, Customer customer)
        {
            _context.Customers.ReplaceOne(c => c.Id == id, customer);
        }

        public void RemoveCustomer(string id)
        {
            _context.Customers.DeleteOne(c => c.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend_task_one__online_store_api_.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Backend_task_one__online_store_api_.Persistence
{
    public class AppDbContext
    {
        public readonly IMongoCollection<Customer> Customers;
        public readonly IMongoCollection<Product> Products;
        public readonly IMongoCollection<Vendor> Vendors;
        public readonly IMongoCollection<Order> Orders;
       

     
        public AppDbContext(IMongoClient client)
        {
            var database = client.GetDatabase("EStore");
            Customers = database.GetCollection<Customer>("Customers");

            // Created An Index for Fullname Field
            var customerKey = Builders<Customer>.IndexKeys;
            var customerIndexModel = new CreateIndexModel<Customer>(customerKey.Text(_ => _.FullName));
            Customers.Indexes.CreateOne(customerIndexModel);

            Products = database.GetCollection<Product>("Products");

            // Created An Index for sku, tags, Category Field respectively
            var productKey = Builders<Product>.IndexKeys.Ascending(_ => _.Sku).Ascending(_ => _.Category);
            var productIndexModel =  new CreateIndexModel<Product>(productKey.Text(_ => _.Name));
            Products.Indexes.CreateOne(productIndexModel);


            Vendors = database.GetCollection<Vendor>("Vendors");
            var vendorKey = Builders<Vendor>.IndexKeys.Text(_ => _.Name);
            var vendorIndexModel = new CreateIndexModel<Vendor>(vendorKey);
            Vendors.Indexes.CreateOne(vendorIndexModel);

            Orders = database.GetCollection<Order>("Orders");
        }

      
       
    }
}

﻿using System;
using Backend_task_one__online_store_api_.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

public class Vendor
{
    public Vendor()
    {
        CreatedAt = DateTime.Now;
        LastUpdate = DateTime.Now;

    }

    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public  string Id { get; set; }
       
      
    [BsonElement("Name")]
    public string  Name { get; set; }

    [BsonElement("Location")]
    public Address Location { get; set; }

    [BsonElement("UpdatedAt")] 
    public DateTime LastUpdate { get; set; } 

    [BsonElement("CreatedAt")]
    public DateTime CreatedAt { get; set; }
}
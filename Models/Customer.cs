﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Backend_task_one__online_store_api_.Models
{
    public class Customer
    {
     
        public Customer()
        {
            OrderIds = new List<string>();
            CreatedAt = DateTime.Now;
            LastUpdate = DateTime.Now;

        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public  string Id { get; set; }
       
      
        [BsonElement("Name")]
        public string  FullName { get; set; }


        [BsonElement("Address")]
        public Address Address { get; set; }

        [BsonElement("BillingAddress")]
        public Address BillingAddress { get; set; }

        [BsonElement("Order_ids")]
        public List<string> OrderIds { get; set; }

        [BsonElement("UpdatedAt")] 
        public DateTime LastUpdate { get; set; } 

        [BsonElement("CreatedAt")]
        public DateTime CreatedAt { get; set; }
    }
}
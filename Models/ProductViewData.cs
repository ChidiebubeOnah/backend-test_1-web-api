﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Backend_task_one__online_store_api_.Models
{
    public class ProductViewData
    {
        public ProductViewData()
        {
            
        }
        public ProductViewData(Product product, Vendor vendor)
        {
            Id = product.Id;
            Sku = product.Sku;
            Name = product.Name;
            Vendor = vendor;
            Category = product.Category;
            Tags = product.Tags;
            Quantity = product.Quantity;
            Price = product.Price;
            TotalOrder = product.TotalOrder;
            LastUpdate = product.LastUpdate;
            CreatedAt = product.CreatedAt;

        }



        public string Id { get;}

      
        public string Sku { get;}


        public string Name { get;  }

      
        public Vendor Vendor { get; }

      
        public Category Category { get;}

       
        public List<Tag> Tags { get;  }

        
        public int Quantity { get; }

       
        public double Price { get; }

        public int TotalOrder { get; }

        public DateTime LastUpdate { get; }

        public DateTime CreatedAt { get; }

    }
}
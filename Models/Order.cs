﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Backend_task_one__online_store_api_.Models
{
    public class Order
    {
        public Order()
        {
            CreatedAt = DateTime.Now;
            LastUpdate = DateTime.Now;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("CustomerId")]
        public string CustomerId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("ProductId")]
        public string ProductId { get; set; }

        [BsonElement("Quantity")]
        public int  Quantity { get; set; }

        [BsonElement("Total")]
        public double Total { get; set; }

        
        [BsonElement("UpdatedAt")]
        public DateTime LastUpdate { get; set; }

        [BsonElement("CreatedAt")]
        public DateTime CreatedAt { get; set; }


    }
}
﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Backend_task_one__online_store_api_.Models
{
    public class Product{

        public Product()
        {
            Tags = new List<Tag>();
            CreatedAt = DateTime.Now;
            LastUpdate = DateTime.Now;
        }


        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]

        public string Id { get; set; }

        [BsonElement("Sku")]
        public string Sku { get; set; }


        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("VendorId")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string VendorId { get; set; }

        [BsonElement("Category")]
        public Category Category { get; set; }

        [BsonElement("Tags")]
        public List<Tag> Tags { get; set; }

        [BsonElement("Instock")]
        public int Quantity { get; set; }

        [BsonElement("Price")]
        public double Price { get; set; }

        [BsonElement("Sales")]
        public int TotalOrder { get; set; }

        [BsonElement("UpdatedAt")]
        public DateTime LastUpdate { get; set; }

        [BsonElement("CreatedAt")]
        public DateTime CreatedAt { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Backend_task_one__online_store_api_.Dtos;
using Backend_task_one__online_store_api_.Dtos.Customer;
using Backend_task_one__online_store_api_.Dtos.Vendors;
using Backend_task_one__online_store_api_.Models;

namespace Backend_task_one__online_store_api_.Profiles
{
    public class CustomerProfile:Profile
    {
        public CustomerProfile()
        {
            CreateMap<CreateCustomerDto, Customer>();

            CreateMap<UpdateCustomerDto, Customer>();
            CreateMap<Customer, UpdateCustomerDto>();
        }
    }
}

﻿using AutoMapper;
using Backend_task_one__online_store_api_.Dtos;
using Backend_task_one__online_store_api_.Dtos.Vendors;

namespace Backend_task_one__online_store_api_.Profiles
{
    public class VendorProfile : Profile
    {
        public VendorProfile()
        {

            // mapping  CreateVendorDto to Vendor
            CreateMap<CreateVendorDto, Vendor>();

            // mapping  UpdateVendorDto to Vendor
            CreateMap<UpdateVendorDto, Vendor>();

            // mapping Vendor to  UpdateVendorDto
            CreateMap<Vendor, UpdateVendorDto>();
        }
    }
}
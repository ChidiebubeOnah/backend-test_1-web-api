﻿using System.Collections;
using System.Collections.Generic;
using Backend_task_one__online_store_api_.Models;

namespace Backend_task_one__online_store_api_.Interfaces
{
    public interface IProductRepository
    {

        IEnumerable<ProductViewData> GetProductsViewData();
        IEnumerable<ProductViewData> GetProductsViewData(string search);
        Product GetProductById(string id);
        ProductViewData GetProductViwDataById(string id);
        void AddProduct(Product product);
        void UpdateProduct(string id, Product product);
        void RemoveProduct(string id);

        IEnumerable GetProductCategorySales();
    }
}
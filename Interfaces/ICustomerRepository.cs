﻿using System.Collections.Generic;
using Backend_task_one__online_store_api_.Models;

namespace Backend_task_one__online_store_api_.Interfaces
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetCustomers();
        IEnumerable<Customer> GetCustomers(string search);
        Customer GetCustomerById(string id);
        void CreateCustomer(Customer customer);
        void UpdateCustomer(string id, Customer customer);
        void RemoveCustomer(string id);
    }
}

﻿using System.Collections.Generic;

namespace Backend_task_one__online_store_api_.Interfaces
{
    public interface IVendorRepository
    {

        IEnumerable<Vendor> GetVendors();
        IEnumerable<Vendor> GetVendors(string search);
        Vendor GetVendorById(string id);
        void CreateVendor(Vendor vendor);
        void UpdateVendor(string id, Vendor vendor);
        void RemoveVendor(string id);
    }
}

    

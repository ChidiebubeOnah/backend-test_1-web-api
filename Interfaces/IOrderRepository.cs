﻿using System.Collections.Generic;
using Backend_task_one__online_store_api_.Models;

namespace Backend_task_one__online_store_api_.Interfaces
{
    public interface IOrderRepository
    {
        IEnumerable<Order> GetOrders();
        Order GetOrder(string orderId );
        (bool success, string msg) MakeOrder(Order order);
        void UpdateOrder(string id, Order order);
        void RemoveOrder(string id);
    }
}
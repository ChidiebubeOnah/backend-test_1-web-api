﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Backend_task_one__online_store_api_.Dtos.Orders
{
    public class CreateOrderDto
    {
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("CustomerId")]
        public string CustomerId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("ProductId")]
        public string ProductId { get; set; }

        [BsonElement("Quantity")]
        public int Quantity { get; set; }


    }
}
﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Backend_task_one__online_store_api_.Dtos.Orders
{
    public class UpdateOrderDto
    {

        [BsonElement("Quantity")]
        public int Quantity { get; set; }

        [BsonElement("UpdatedAt")]
        public DateTime LastUpdate { get; set; } = DateTime.Now;

    }
}
﻿using Backend_task_one__online_store_api_.Models;
using MongoDB.Bson.Serialization.Attributes;

namespace Backend_task_one__online_store_api_.Dtos.Vendors
{
    public class CreateVendorDto
    {

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Location")]
        public Address Location { get; set; }
    }
}
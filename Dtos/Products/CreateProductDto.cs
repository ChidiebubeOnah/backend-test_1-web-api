﻿using System.Collections.Generic;
using Backend_task_one__online_store_api_.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Backend_task_one__online_store_api_.Dtos.Products
{
    public class CreateProductDto
    {
        public CreateProductDto()
        {
            Tags = new List<Tag>();
        }
        
        [BsonElement("Sku")]
        public string Sku { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("VendorId")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string VendorId { get; set; }

        [BsonElement("Category")]
        public Category Category { get; set; }

        [BsonElement("Tags")]
        public List<Tag> Tags { get; set; }

        [BsonElement("Instock")]
        public int Quantity { get; set; }

        [BsonElement("Price")]
        public double Price { get; set; }

    }
}
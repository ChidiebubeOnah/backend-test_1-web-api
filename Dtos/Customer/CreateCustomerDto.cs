﻿using Backend_task_one__online_store_api_.Models;
using MongoDB.Bson.Serialization.Attributes;

namespace Backend_task_one__online_store_api_.Dtos.Customer
{
    public class CreateCustomerDto
    {

        [BsonElement("Name")]
        public string FullName { get; set; }


        [BsonElement("Address")]
        public Address Address { get; set; }

        [BsonElement("BillingAddress")]
        public Address BillingAddress { get; set; }
    }
}

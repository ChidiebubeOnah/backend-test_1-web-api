﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend_task_one__online_store_api_.Interfaces;
using Backend_task_one__online_store_api_.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Backend_task_one__online_store_api_.Controllers
{


    [Route("api/sales")]
    [ApiController]
    public class SalesController: ControllerBase
    {
        private readonly IProductRepository _repository;

        public SalesController(IProductRepository repository)
        {
            _repository = repository;
        }

        // GET api/sales
        [HttpGet]
        public IActionResult GetSalesByCate()
        {
            return Ok(_repository.GetProductCategorySales());
        }
        }
}

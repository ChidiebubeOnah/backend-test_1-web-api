﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Backend_task_one__online_store_api_.Dtos;
using Backend_task_one__online_store_api_.Dtos.Customer;
using Backend_task_one__online_store_api_.Dtos.Vendors;
using Backend_task_one__online_store_api_.Interfaces;
using Backend_task_one__online_store_api_.Models;
using Backend_task_one__online_store_api_.Repositories;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace Backend_task_one__online_store_api_.Controllers
{
   
    [Route("api/customers")]
    [ApiController]
    public class CustomersController: ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;


        public CustomersController(ICustomerRepository customerRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }
        
        
        // Get api/customers
        // or
        // Get api/customers?search=fullname

        [HttpGet]
        public ActionResult<IEnumerable<Customer>> GetAllCustomers(string search)
        {
            var customers = _customerRepository.GetCustomers();

            if (string.IsNullOrWhiteSpace(search))
            {
                return Ok(customers);

            }
            customers = _customerRepository.GetCustomers(search);

            if (customers.ToList().Count == 0)
            {
                return NotFound();
            }
            return Ok(customers);

        }

        // Get api/customers/{id}
        [HttpGet("{id}", Name="CustomersById")]
        public ActionResult<Customer> GetCustomerById(string id)
        {
            var customer = _customerRepository.GetCustomerById(id);
            if (customer == null)
            {
                return NotFound();
            }

            return customer;
        }

        // POST api/customers
        [HttpPost]
        public ActionResult<Customer> CreateCustomer(CreateCustomerDto customer)
        {

            var customerModel = _mapper.Map<Customer>(customer);
            _customerRepository.CreateCustomer(customerModel);
            return CreatedAtRoute("CustomersById", new {Id = customerModel.Id}, customerModel);
        }  

        
        // PUT api/customers/{id}

        [HttpPut("{id}")]
        public ActionResult UpdateCustomer(string id, UpdateCustomerDto customerDto)
        {
            var customer = _customerRepository.GetCustomerById(id);
            if (customer == null)
            {
                return NotFound();
            }

            customer = _mapper.Map(customerDto, customer);

            _customerRepository.UpdateCustomer(id,customer);

            return NoContent();
        }

        // PATCH api/customers/{id}
        [HttpPatch("{id}")]
        public ActionResult PartialCustomerUpdate(string id, JsonPatchDocument<UpdateCustomerDto> patchDto)
        {
            var customerFromDb = _customerRepository.GetCustomerById(id);
            if (customerFromDb == null)
            {
                return NotFound();
            }

            var customerToPatch = _mapper.Map<UpdateCustomerDto>(customerFromDb);
            patchDto.ApplyTo(customerToPatch,ModelState);
            if (!TryValidateModel(customerToPatch))
            {
                return ValidationProblem(ModelState);
            }

            _mapper.Map(customerToPatch, customerFromDb);
            _customerRepository.UpdateCustomer(id,customerFromDb);

            return NoContent();
        }


        // DELETE api/customers/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteCustomer(string id)
        {
            var customer = _customerRepository.GetCustomerById(id);
            if (customer == null)
            {
                return NotFound();
            }

            _customerRepository.RemoveCustomer(customer.Id);
            return NoContent();
        }
    }
}


// Debug for Patch endpoint

//
// [{
//     "op": "replace",
//     "path": "/address",
//     "value": {
//         "streetAddress": "Swiss Patch",
//         "city": "London",
//         "state": "UK",
//         "zipCode": "abc009"
//     }
//  
//   
//       
// }]
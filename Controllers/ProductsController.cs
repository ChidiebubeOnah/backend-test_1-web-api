﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Backend_task_one__online_store_api_.Dtos;
using Backend_task_one__online_store_api_.Dtos.Products;
using Backend_task_one__online_store_api_.Interfaces;
using Backend_task_one__online_store_api_.Models;
using Backend_task_one__online_store_api_.Repositories;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace Backend_task_one__online_store_api_.Controllers
{


    [Route("api/products")]
    [ApiController]
    public class ProductsController: ControllerBase
    {
        private readonly IProductRepository _repository;
        private readonly IVendorRepository _vendorRepository;
        private readonly IMapper _mapper;

        public ProductsController(IProductRepository repository,IVendorRepository vendorRepository, IMapper mapper)
        {
            _repository = repository;
            _vendorRepository = vendorRepository;
            _mapper = mapper;
        }

        // Get api/products
        // or
        // Get api/products?search=keyword

        [HttpGet]
        public ActionResult<IEnumerable<ProductViewData>> GetAllProducts(string search)
        {
            var products = _repository.GetProductsViewData();

            if (string.IsNullOrWhiteSpace(search))
            {
                return Ok(products);

            }
            products = _repository.GetProductsViewData(search);

            if (products.ToList().Count == 0)
            {
                return NotFound();
            }
            return Ok(products);

        }

        // Get api/products/{id}
        [HttpGet("{id}", Name = "ProductById")]
        public ActionResult<ProductViewData> GetProductById(string id)
        {
            var product = _repository.GetProductViwDataById(id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        // POST api/products
        [HttpPost]
        public ActionResult<Product> CreateProduct(CreateProductDto productDto)
        {

            var vendor = _vendorRepository.GetVendorById(productDto.VendorId);
            if (vendor == null)
            {
                return BadRequest("VendorId does not Exist");
            }
            var productModel = _mapper.Map<Product>(productDto);
            _repository.AddProduct(productModel);
            return CreatedAtRoute("ProductById", new { Id = productModel.Id }, productModel);
        }

        // PUT api/products/{id}

        [HttpPut("{id}")]
        public ActionResult UpdateProduct(string id, UpdateProductDto productDto)
        {
            var product = _repository.GetProductById(id);
            if (product == null)
            {
                return NotFound();
            }

            var vendor = _vendorRepository.GetVendorById(productDto.VendorId);
            if (vendor == null)
            {
                return BadRequest("VendorId does not Exist");
            }

            product = _mapper.Map(productDto, product);

            _repository.UpdateProduct(id, product);

            return NoContent();
        }

        // PATCH api/products/{id}
        [HttpPatch("{id}")]
        public ActionResult PartialProductUpdate(string id, JsonPatchDocument<UpdateProductDto> patchDto)
        {
            var productFromDb = _repository.GetProductById(id);
            if (productFromDb == null)
            {
                return NotFound();
            }

            var productToPatch = _mapper.Map<UpdateProductDto>(productFromDb);
            patchDto.ApplyTo(productToPatch, ModelState);
            if (!TryValidateModel(productToPatch))
            {
                return ValidationProblem(ModelState);
            }

            var vendor = _vendorRepository.GetVendorById(productToPatch.VendorId);
            if (vendor == null)
            {
                return BadRequest("VendorId does not Exist");
            }
            _mapper.Map(productToPatch, productFromDb);
            _repository.UpdateProduct(id,productFromDb);

            return NoContent();
        }



        // DELETE api/customers/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteProduct(string id)
        {
            var product = _repository.GetProductById(id);
            if (product == null)
            {
                return NotFound();
            }

            _repository.RemoveProduct(product.Id);
            return NoContent();
        }


    }
}

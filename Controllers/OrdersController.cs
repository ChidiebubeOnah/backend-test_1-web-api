﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Backend_task_one__online_store_api_.Dtos;
using Backend_task_one__online_store_api_.Dtos.Orders;
using Backend_task_one__online_store_api_.Interfaces;
using Backend_task_one__online_store_api_.Models;
using Backend_task_one__online_store_api_.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Backend_task_one__online_store_api_.Controllers
{
    [Route("api/orders")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderRepository _repository;
        private readonly IMapper _mapper;

        public OrdersController(IOrderRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }



        // Get api/orders

        [HttpGet]
        public ActionResult<IEnumerable<Order>> GetAllOrders()
        {
            var orders = _repository.GetOrders();


            if (!orders.Any())
            {
                return NotFound();
            }
            return Ok(orders);

        }


        // Get api/orders/{id}

        [HttpGet("{id}", Name = "OrderById")]
        public ActionResult<Order> GetOrderById(string id)
        {
            var order = _repository.GetOrder(id);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(order);
        }


        // POST api/orders
        [HttpPost]
        public ActionResult<Order> CreateOrder(CreateOrderDto createOrderDto)
        {

            var orderModel = _mapper.Map<Order>(createOrderDto);

           
            if (_repository.MakeOrder(orderModel).success)
            {
                return CreatedAtRoute("OrderById", new { Id = orderModel.Id }, orderModel);
            }

            return BadRequest(_repository.MakeOrder(orderModel).msg);
        }



        // PUT api/orders/{id}

        [HttpPut("{id}")]
        public ActionResult UpdateOrder(string id, UpdateOrderDto orderDto)
        {
            var order = _repository.GetOrder(id);
            if (order == null)
            {
                return NotFound();
            }

            order = _mapper.Map(orderDto, order);

            _repository.UpdateOrder(id, order);

            return NoContent();
        }

        // DELETE api/orders/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteOrder(string id)
        {
            var order = _repository.GetOrder(id);
            if (order == null)
            {
                return NotFound();
            }

            _repository.RemoveOrder(order.Id);
            return NoContent();
        }

    }
}
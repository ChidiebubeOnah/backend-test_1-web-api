﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Backend_task_one__online_store_api_.Dtos;
using Backend_task_one__online_store_api_.Dtos.Vendors;
using Backend_task_one__online_store_api_.Interfaces;
using Backend_task_one__online_store_api_.Repositories;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace Backend_task_one__online_store_api_.Controllers
{
    [Route("api/vendors")]
    [ApiController]
    public class VendorsController: ControllerBase
    {
        private readonly IVendorRepository _repository;
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;

        public VendorsController(IVendorRepository repository, IProductRepository productRepository, IMapper mapper)
        {
            _repository = repository;
            _productRepository = productRepository;
            _mapper = mapper;
        }

        // Get api/vendors
        // or
        // Get api/vendors?search=name

        [HttpGet]
        public ActionResult<IEnumerable<Vendor>> GetAllVendors(string search)
        {
            var vendors = _repository.GetVendors();

            if (string.IsNullOrWhiteSpace(search))
            {
                return Ok(vendors);

            }
            vendors = _repository.GetVendors(search);

            if (!vendors.Any())
            {
                return NotFound();
            }
            return Ok(vendors);

        }

        // Get api/vendors/{id}
        [HttpGet("{id}", Name = "VendorById")]
        public ActionResult<Vendor> GetVendorById(string id)
        {
            var vendor = _repository.GetVendorById(id);
            if (vendor == null)
            {
                return NotFound();
            }

            return vendor;
        }

        // POST api/vendors
        [HttpPost]
        public ActionResult<Vendor> CreateVendor(CreateVendorDto vendorDto)
        {
            var vendor = _mapper.Map<Vendor>(vendorDto);
            _repository.CreateVendor(vendor);
            return CreatedAtRoute("VendorById", new { Id = vendor.Id }, vendor);
        }


        // PUT api/vendors/{id}

        [HttpPut("{id}")]
        public ActionResult UpdateVendor(string id, UpdateVendorDto vendorDto)
        {
            var vendor = _repository.GetVendorById(id);

            if (vendor == null)
            {
                return NotFound();
            }

            vendor = _mapper.Map(vendorDto, vendor);

           _repository.UpdateVendor(id, vendor);

            return NoContent();
        }

        // PATCH api/vendors/{id}
        [HttpPatch("{id}")]
        public ActionResult PartialVendorUpdate(string id, JsonPatchDocument<UpdateVendorDto> patchDto)
        {
            var vendorFromDb = _repository.GetVendorById(id);
            if (vendorFromDb == null)
            {
                return NotFound();
            }

            var vendorToPatch = _mapper.Map<UpdateVendorDto>(vendorFromDb);
            patchDto.ApplyTo(vendorToPatch, ModelState);
            if (!TryValidateModel(vendorToPatch))
            {
                return ValidationProblem(ModelState);
            }

            _mapper.Map(vendorToPatch, vendorFromDb);
            _repository.UpdateVendor(id, vendorFromDb);

            return NoContent();
        }

        // DELETE api/vendors/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteVendor(string id)
        {
            var vendor = _repository.GetVendorById(id);
            if (vendor == null)
            {
                return NotFound();
            }
            _repository.RemoveVendor(vendor.Id);
            return NoContent();
        }



    }
}